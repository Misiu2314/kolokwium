//zad 1


#include"string"

using namespace std;

class atrakcja
{
protected:
	int cena;
	string nazwa;
	string opis;
	void constructor();
public:
int getCena();
string getNazwa();
string getOpis();


};


class kolejka
	:protected atrakcja
{
private:
int	godz_odjazdu;
int godz_przyjazdu;
public:
void inicjuj(int new_godz_odjazdu,int new_godz_przyjazdu);
};


class zamek
	:protected atrakcja
{
private:
	int czas_zwiedzania;
public:
void inicjuj(int new_czas_zwiedzania);
	
};

class film
	:protected atrakcja
{
private:
	int czas_trwania;
	string tytul;
public:
void inicjuj(int new_czas_trwania);
};

